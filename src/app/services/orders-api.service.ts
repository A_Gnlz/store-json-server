import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class OrdersApiService {

  baseURL: string = environment.baseURL;

  constructor(private httpClient: HttpClient) { }

  getOrders(){
    return this.httpClient.get(`${this.baseURL}/orders`).pipe(
      map(res => res)
    );
  }

  postOrder(data: any){
    return this.httpClient.post(`${this.baseURL}/orders`, data).pipe(
      map(res => res)
    );
  }
}
