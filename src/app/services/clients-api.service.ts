import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators'

@Injectable({
  providedIn: 'root'
})
export class ClientsApiService {

  baseURL: string = environment.baseURL;

  constructor(private httpClient: HttpClient) { }

  getClients(){
    return this.httpClient.get(`${this.baseURL}/clients`).pipe(
      map(res => res)
    );
  }

  getClient(id: number){
    return this.httpClient.get(`${this.baseURL}/clients/${id}`).pipe(
      map(res => res)
    );
  }

  postClient(data: any){
    return this.httpClient.post(`${this.baseURL}/clients`, data).pipe(
      map(res => res)
    );
  }
}
