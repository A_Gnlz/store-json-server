import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ProductsApiService {

  baseURL: string = environment.baseURL;

  constructor(private httpClient: HttpClient) { }

  getProducts(){
    return this.httpClient.get(`${this.baseURL}/products`).pipe(
      map(res => res)
    );
  }

  getProduct(id: number){
    return this.httpClient.get(`${this.baseURL}/products/${id}`).pipe(
      map(res => res)
    );
  }

  postProduct(data: any){
    return this.httpClient.post(`${this.baseURL}/products`, data).pipe(
      map(res => res)
    );
  }

}
