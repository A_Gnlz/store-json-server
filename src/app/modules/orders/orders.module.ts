import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { OrdersRoutingModule } from './orders-routing.module';
import { OrdersListComponent } from './components/orders-list/orders-list.component';
import { CreateOrderComponent } from './components/create-order/create-order.component';


@NgModule({
  declarations: [OrdersListComponent, CreateOrderComponent],
  imports: [
    CommonModule,
    OrdersRoutingModule
  ]
})
export class OrdersModule { }
