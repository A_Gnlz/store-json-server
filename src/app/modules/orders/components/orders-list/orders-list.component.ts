import { Component, OnInit } from '@angular/core';
import { OrdersApiService } from 'src/app/services/orders-api.service';

@Component({
  selector: 'app-orders-list',
  templateUrl: './orders-list.component.html',
  styleUrls: ['./orders-list.component.scss']
})
export class OrdersListComponent implements OnInit {

  ordersInfo: any;

  constructor(private orderService: OrdersApiService) { }

  ngOnInit(): void {
    this.getOrdersInfo();
  }

  getOrdersInfo(){
    this.orderService.getOrders().subscribe(
      res=>{
        this.ordersInfo = res;
        console.log(res);
      }, err=>{
        console.error(err);
      }
    )
  }

}
