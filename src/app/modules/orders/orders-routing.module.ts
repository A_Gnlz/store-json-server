import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from '../../core/dashboard/dashboard.component';
import { CreateOrderComponent } from './components/create-order/create-order.component';
import { OrdersListComponent } from './components/orders-list/orders-list.component';

const routes: Routes = [
  {
    path: 'ordenes',
    component: DashboardComponent,
    children: [
      {
        path: '',
        component: OrdersListComponent
      },
      {
        path: 'crear-orden',
        component: CreateOrderComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OrdersRoutingModule { }
