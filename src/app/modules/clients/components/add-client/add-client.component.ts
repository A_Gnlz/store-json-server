import { Component, OnInit, Type } from '@angular/core';
import { FormArray, FormBuilder, Validators} from '@angular/forms';
import { ClientsApiService } from 'src/app/services/clients-api.service';

@Component({
  selector: 'app-add-client',
  templateUrl: './add-client.component.html',
  styleUrls: ['./add-client.component.scss']
})
export class AddClientComponent implements OnInit {

  clientForm = this.formBuilder.group({
    clientArray: this.formBuilder.array([])
  })

  constructor(private clientService: ClientsApiService, private formBuilder: FormBuilder) { }

  ngOnInit(): void {
    this.addControlsSet();
  }

  get clientArray(){
    return this.clientForm.controls["clientArray"] as FormArray;
  }

  addControlsSet(){
    const clientControlsSet = this.formBuilder.group({
      firstName: ['', [Validators.required]],
      lastName: ['', [Validators.required]],
      phoneNumber: ['', [Validators.required, Validators.maxLength(8), Validators.minLength(8)]],
      email: ['', [Validators.required]],
      nit: ['', [Validators.required, Validators.maxLength(14), Validators.minLength(14)]]
    })

    this.clientArray.push(clientControlsSet);
    console.log(this.clientArray)
  }

  onSubmit(){
    for(let item of this.clientForm.value.clientArray){
      this.clientService.postClient(item).subscribe(
        res=>{
          console.log(res);
        }, err=>{
          console.error(err);
        }
      )
    }
  }

}
