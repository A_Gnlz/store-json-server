import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ClientsApiService } from 'src/app/services/clients-api.service';

@Component({
  selector: 'app-client-detail',
  templateUrl: './client-detail.component.html',
  styleUrls: ['./client-detail.component.scss']
})
export class ClientDetailComponent implements OnInit {

  idParam: number = 0;
  clientInfo: any;

  constructor(private clientsService: ClientsApiService, private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.idParam = Number(this.route.snapshot.paramMap.get('id'));
    this.getClientInfo(this.idParam);
  }

  getClientInfo(id: number){
    this.clientsService.getClient(id).subscribe(
      res=>{
        this.clientInfo = res;
        console.log('client detail', res);
      }, err=>{
        console.error('client detail err', err);
      }
    )
  }

  back(){
    history.back();
  }

}
