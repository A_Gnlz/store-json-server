import { Component, OnInit } from '@angular/core';
import { ClientsApiService } from 'src/app/services/clients-api.service';

@Component({
  selector: 'app-clients-list',
  templateUrl: './clients-list.component.html',
  styleUrls: ['./clients-list.component.scss']
})
export class ClientsListComponent implements OnInit {

  clientInfo: any;

  constructor(private clientsService: ClientsApiService) { }

  ngOnInit(): void {
    this.getClientsInfo();
  }

  getClientsInfo(){
    this.clientsService.getClients().subscribe(
      res=>{
        this.clientInfo = res;
        console.log(res);
      }, err=>{
        console.error(err);
      }
    )
  }

}
