import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from '../../core/dashboard/dashboard.component';
import { AddClientComponent } from './components/add-client/add-client.component';
import { ClientDetailComponent } from './components/client-detail/client-detail.component';
import { ClientsListComponent } from './components/clients-list/clients-list.component';

const routes: Routes = [
  {
    path: 'clientes',
    component: DashboardComponent,
    children: [
      {
        path: '',
        component: ClientsListComponent
      },
      {
        path: 'agregar',
        component: AddClientComponent
      },
      {
        path: 'mostrar/:id',
        component: ClientDetailComponent
      }
    ]
  }  
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ClientsRoutingModule { }
