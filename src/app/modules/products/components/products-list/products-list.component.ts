import { Component, OnInit } from '@angular/core';
import { ProductsApiService } from 'src/app/services/products-api.service';

@Component({
  selector: 'app-products-list',
  templateUrl: './products-list.component.html',
  styleUrls: ['./products-list.component.scss']
})
export class ProductsListComponent implements OnInit {

  productsInfo: any;

  constructor(private productsService: ProductsApiService) { }

  ngOnInit(): void {
    this.getProductsInfo();
  }

  getProductsInfo(){
    this.productsService.getProducts().subscribe(
      res=>{
        this.productsInfo = res;
        console.log(res);
      },err=>{
        console.log(err);
      }
    )
  }

}
