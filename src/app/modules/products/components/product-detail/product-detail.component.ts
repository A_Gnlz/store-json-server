import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ProductsApiService } from 'src/app/services/products-api.service';

@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.scss']
})
export class ProductDetailComponent implements OnInit {

  idParam: number = 0;
  productInfo: any;

  constructor(private productsService: ProductsApiService, private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.idParam = Number(this.route.snapshot.paramMap.get('id'));
    this.getClientInfo(this.idParam);
  }

  getClientInfo(id: number){
    this.productsService.getProduct(id).subscribe(
      res=>{
        this.productInfo = res;
        console.log('client detail', res);
      }, err=>{
        console.error('client detail err', err);
      }
    )
  }

  back(){
    history.back();
  }

}
