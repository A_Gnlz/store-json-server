import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, Validators} from '@angular/forms';
import { ClientsApiService } from 'src/app/services/clients-api.service';
import { ProductsApiService } from 'src/app/services/products-api.service';

@Component({
  selector: 'app-add-product',
  templateUrl: './add-product.component.html',
  styleUrls: ['./add-product.component.scss']
})
export class AddProductComponent implements OnInit {

  productForm = this.formBuilder.group({
    productArray: this.formBuilder.array([])
  })

  constructor(private productsService: ProductsApiService, private formBuilder: FormBuilder) { }

  ngOnInit(): void {
    this.addControlsSet();
  }

  get productArray(){
    return this.productForm.controls["productArray"] as FormArray;
  }

  addControlsSet(){
    const productControlSet = this.formBuilder.group({
      name: ['', [Validators.required]],
      description: ['', [Validators.required]],
      cost: ['', [Validators.required]]
    })

    this.productArray.push(productControlSet);
    console.log(this.productArray)
  }

  onSubmit(){
    for(let item of this.productForm.value.productArray){
      this.productsService.postProduct(item).subscribe(
        res=>{
          console.log(res);
        }, err=>{
          console.error(err);
        }
      )
    }
  }

}
